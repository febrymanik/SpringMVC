<?php

namespace backend\controllers;

use Yii;
use backend\models\IzinBermalam;
use backend\models\IzinBermalamSearch;
use backend\models\Mahasiswa;
use backend\models\MahasiswaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use mPDF;

/**
 * IzinBermalamController implements the CRUD actions for IzinBermalam model.
 */
class IzinBermalamController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
        'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login','view', 'create', 'printform','update','accept', 'batal', 'reject', 'delete', 'createizinmahasiswa', 'indexmahasiswa'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],            

            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all IzinBermalam models.
     * @return mixed
     */
    public function actionIndexmahasiswa()
    {
        $searchModel = new IzinBermalamSearch();
        $dataProvider = $searchModel->searchlogin(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

       public function actionAccept($id)
    {
        
        
        $accept = $this->findModel($id);
        $accept->status = 'Accepted';
        $accept->nid_keasramaan = Yii::$app->user->identity->username;
        $accept->save(false);

        return $this->redirect(['index']);          
    }

        public function actionBatal($id)
    {
        
        
        $accept = $this->findModel($id);
        $accept->status = 'Batal';
        $accept->nid_keasramaan = '';
        $accept->save(false);

        return $this->redirect(['indexmahasiswa']);          
    }

    public function actionReject($id)
    {    
            $model = $this->findModel($id);
            
            $model->status = 'Reject';
            $model->nid_keasramaan = Yii::$app->user->identity->username;
            $model->save(false);
     
            return $this->redirect(['index']);    
    }  


    public function actionIndex()
    {
        $searchModel = new IzinBermalamSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionLogIzinmahasiswa()
    {
        $searchModel = new IzinBermalamSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreateizinmahasiswa()
    {
        $model = new IzinBermalam();
        $mahasiwa = Mahasiswa::findOne(Yii::$app->user->identity->username);

        if ($model->load(Yii::$app->request->post())) {
            $model->nim = $mahasiwa->nim;
            $model->nama = $mahasiwa->nama;
            $model->no_telepon_orangtua = $mahasiwa->notlponortu;        
             if($model->save() ){
                return $this->redirect(['view', 'id' => $model->no]);    
             }          
            
        } else {
            return $this->render('createijinbermalam', [
                'model' => $model,
            ]);
        }
    }
    /**
     * Displays a single IzinBermalam model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    /**
     * Creates a new IzinBermalam model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new IzinBermalam();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->no]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing IzinBermalam model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->no]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing IzinBermalam model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the IzinBermalam model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return IzinBermalam the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */


    public function actionPrintform($id){      
        $formib = $this->findModel($id);
        $htmlprint = '
        <HTML>
        <HEAD>
        </HEAD>

        <BODY>
        <DIV id="page_1">
            <DIV id="p1dimg1">
                <IMG src="" alt=""></DIV>


                <DIV id="id_1">
                <TABLE cellpadding=0 cellspacing=0 class="t0">
                <TR>
                    <TD colspan=3 class="tr0 td0"><P class="p0 ft0">SURAT IZIN BERMALAM MAHASISWA</P></TD>
                    <TD class="tr0 td1"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr0 td2"><P class="p2 ft2">Institut Teknologi Del</P></TD>
                </TR>
                <TR>
                    <TD class="tr1 td3"><P class="p1 ft3">&nbsp;</P></TD>
                    <TD class="tr1 td4"><P class="p1 ft3">&nbsp;</P></TD>
                    <TD class="tr2 td5"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr2 td1"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr2 td2"><P class="p2 ft4">Jl. Sisingamangaraja</P></TD>
                </TR>
                <TR>
                    <TD class="tr3 td6"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr3 td7"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr3 td5"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr3 td1"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr3 td2"><P class="p2 ft5">Desa <NOBR>Sitoluama-Kec.</NOBR> Laguboti</P></TD>
                </TR>
                <TR>
                    <TD class="tr4 td6"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr4 td7"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr4 td5"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr4 td1"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr4 td2"><P class="p2 ft6">Kab. Tobasa, Sumatera Utara,Indonesia</P></TD>
                </TR>
                <TR>
                    <TD class="tr3 td6"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr3 td7"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr3 td5"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr3 td1"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr3 td2"><P class="p2 ft5">Telp : (0632) 331234</P></TD>
                </TR>
                <TR>
                    <TD class="tr4 td6"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr4 td7"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr4 td5"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr4 td1"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr4 td2"><P class="p2 ft7">Fax : 331116</P></TD>
                </TR>
                <TR>
                    <TD class="tr3 td6"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr3 td7"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr3 td5"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr3 td1"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr3 td2"><P class="p2 ft5">Website : www.del.ac.id</P></TD>
                </TR>
                <TR>
                    <TD class="tr5 td6"><P class="p1 ft8">DIBERIKAN IZIN BERMALAM KEPADA:</P></TD>
                    <TD class="tr5 td7"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr5 td5"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr5 td1"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr5 td2"><P class="p1 ft1">&nbsp;</P></TD>
                </TR>
                <TR>
                    <TD class="tr6 td6"><P class="p1 ft8">Nama :'.$formib->nama.'</P></TD>
                    <TD class="tr6 td7"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr6 td5"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr6 td1"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr6 td2"><P class="p1 ft1">&nbsp;</P></TD>
                </TR>
                <TR>
                    <TD class="tr7 td6"><P class="p1 ft8">No. HP. Ayah/Ibu/Wali : <NOBR>-/'.$formib->no_telepon_orangtua.'/-</NOBR></P></TD>
                    <TD class="tr7 td7"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr7 td5"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr7 td1"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr7 td2"><P class="p1 ft1">&nbsp;</P></TD>
                </TR>
                <TR>
                    <TD class="tr8 td6"><P class="p1 ft2">Rencana IB</P></TD>
                    <TD class="tr8 td7"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr8 td5"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr8 td1"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr8 td2"><P class="p1 ft1">&nbsp;</P></TD>
                </TR>
                <TR>
                    <TD class="tr7 td6"><P class="p1 ft8">Tanggal berangkat : '.$formib->tanggal_keberangkatan.'</P></TD>  
                    <TD class="tr7 td1"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr7 td2"><P class="p1 ft1">&nbsp;</P></TD>
                </TR>
                <TR>
                    <TD class="tr6 td6"><P class="p1 ft8">Tujuan : '.$formib->tujuan_ib.'</P></TD>
                    <TD class="tr6 td7"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr6 td5"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr6 td1"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr6 td2"><P class="p1 ft1">&nbsp;</P></TD>
                </TR>
                <TR>
                    <TD class="tr7 td6"><P class="p1 ft8">Keperluan : '.$formib->keperluan_ib.'</P></TD>
                    <TD class="tr7 td7"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr7 td5"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr7 td1"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr7 td2"><P class="p1 ft1">&nbsp;</P></TD>
                </TR>
                <TR>
                    <TD class="tr6 td6"><P class="p1 ft8">Tanggal kembali :' .$formib->tanggal_kembali.'</P></TD>  
                    <TD class="tr6 td1"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr6 td2"><P class="p1 ft1">&nbsp;</P></TD>
                </TR>
                <TR>
                    <TD class="tr9 td6"><P class="p3 ft8">Pemohon</P></TD>
                    <TD colspan=2 class="tr9 td8"><P class="p4 ft9">Menyetujui,petugas</P></TD>
                    <TD class="tr9 td1"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr9 td2"><P class="p5 ft8">Diketahui,orangtua/wali</P></TD>
                </TR>
                <TR>
                    <TD class="tr10 td6"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD colspan=2 class="tr10 td8"><P class="p4 ft8">ttd</P></TD>
                    <TD class="tr10 td1"><P class="p1 ft1">&nbsp;</P></TD>
                    <TD class="tr10 td2"><P class="p1 ft1">&nbsp;</P></TD>
                </TR>
                <TR>
                    <TD class="tr10 td6"><P class="p6 ft8">('.$formib->nama.')</P></TD>
                    <TD colspan=2 class="tr10 td8"><P class="p7 ft8">('.$formib->nid_keasramaan.')</P></TD>
                    <TD class="tr10 td1"><P class="p8 ft8">(</P></TD>
                    <TD class="tr10 td2"><P class="p1 ft8">......................................)</P></TD>
                </TR>
                </TABLE>
                <P class="p9 ft10">* Sebelum meninggalkan asrama untuk IB, mahasiswa/i dianjurkan untuk permisi kepada bapak/ibu asrama atau abang/kakak asrama.</P>
                <P class="p10 ft2">Realisasi IB (diisi oleh petugas)</P>
                <TABLE cellpadding=0 cellspacing=0 class="t1">
                <TR>
                    <TD class="tr7 td9"><P class="p1 ft8">Tanggal kembali:</P></TD>
                    <TD class="tr7 td10"><P class="p8 ft8">Pukul:</P></TD>
                </TR>
                <TR>
                    <TD class="tr11 td9"><P class="p1 ft8">Petugas</P></TD>
                    <TD class="tr11 td10"><P class="p1 ft1">&nbsp;</P></TD>
                </TR>
                <TR>
                    <TD class="tr12 td9"><P class="p1 ft11">(..................................</P></TD>
                    <TD class="tr12 td10"><P class="p11 ft8">)</P></TD>
                </TR>
                </TABLE>
                </DIV>
                <DIV id="id_2">
                <TABLE cellpadding=0 cellspacing=0 class="t2">
                <TR>
                    <TD class="tr4 td11"><P class="p1 ft11">Surat ini dicetak melalui aplikasi Asrama dan Kemahasiswaan IT Del</P></TD>
                    <TD class="tr4 td12"><P class="p1 ft11">halaman 1 dari 1</P></TD>
                </TR>
                </TABLE>
            </DIV>
        </DIV>
</HTML>
        ';
        $mpdf=new mPDF();
        $mpdf->WriteHTML($htmlprint);
        $mpdf->Output('Formijinbermalam_'.$formib->nama, 'D');
        exit;
    }       
        
    
    


    protected function findModel($id)
    {
        if (($model = IzinBermalam::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
