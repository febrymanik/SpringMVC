<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use backend\models\Pengumuman;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\RegistrationForm;
use views\site\register;
use views\site\index;


/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'register', 'home'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (\Yii::$app->user->isGuest){
             return $this->redirect(['site/login']);
         }
        $dataProvider= Pengumuman::find()->limit(9)->orderBy('no DESC')->all();
        //$ultahsiswa = \frontend\models\Siswa::find()->all();
        //$ultahguru = \frontend\models\Guru::find()->all();
        
        return $this->render('index',[
            'dataProvider' => $dataProvider,
          //  'siswas' => $ultahsiswa,
            //'gurus' => $ultahguru,
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        
        $this->layout = 'login';
      if (!Yii::$app->user->isGuest) {
          $this->goHome();
      }
    
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }


    public function actionIzinBemalam(){
         $model = new RegistrationForm();
         if($model -> load(Yii::$app->request->post())){         
             if($user =$model->register()){
                return $this->redirect(['site/index']);
             }
         }
         return $this->render('izinBermalam',['model' => $model,]);
     }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {        
        Yii::$app->user->logout();

        return $this->goHome();
    }
    
    public function actionHome(){
      return $this->render('index');
    }
}
