<?php

namespace backend\controllers;

use Yii;
use backend\models\User;
use backend\models\UserSearch;
use backend\models\Satpam;
use backend\models\Mahasiswa;
use backend\models\Keasramaan;
use backend\models\SatpamSearch;
use backend\models\KeasramaanSearch;
use backend\models\MahasiswaSearch;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [          
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login','view', 'createasrama','createmahasiswa', 'createsatpam', 'delete' , 'viewcek',  'updateas' ,'updatesat','updatema', 'updatecekadmin', 'updatemahasiswa', 'updateasrama',  'updatesatpam', 'update', 'updateadmin'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],            

            ], 
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
   

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */



    

    public function actionUpdatecekadmin($id){
        $user = User::findOne($id);

        if($user->role == 'Keasramaan'){
            return $this->redirect(['updateasrama', 'id' => $user->id]);
        }

        else if ($user->role == 'Admin') {
            return $this->redirect(['updateadmin', 'id' => $user->id]);
        }

        else if($user->role == 'Satpam'){
            return $this->redirect(['updatesatpam', 'id' => $user->id]);
        }

        else if ($user->role == 'Mahasiswa') {
           return $this->redirect(['updatemahasiswa', 'id' => $user->id]);
        }        
    }

//OptionUpdate di admin
    public function actionUpdateasrama($id)
    {
        
        $model = $this->findModel($id);
        $modelk = Keasramaan::findOne($model->username);

        if ($model->load(Yii::$app->request->post()) && $modelk->load(Yii::$app->request->post())) {                      
            $hash = Yii::$app->getSecurity()->generatePasswordHash($model->password_hash);
            $model->password_hash = $hash;


            $model->updated_at = date("Y-m-d").' '.date("h:i:s a");
            $model->image = UploadedFile::getInstance($model, 'image');
            $model->image = file_get_contents($model->image->tempName);   
            
            $modelk->nid = $model->username;   

            if($model->save(false) && $modelk->save(false)){
                         
            }
            return $this->redirect(['updateasrama', 'id' => $model->id]);
                                
        } else {
            return $this->render('updateasrama', [
                'model' => $model,
                'modelk' => $modelk,
            ]);
        }
    }


    public function actionUpdatemahasiswa($id)
    {
        
        $model = $this->findModel($id);
        $modelk = Mahasiswa::findOne($model->username);

        if ($model->load(Yii::$app->request->post()) && $modelk->load(Yii::$app->request->post())) {                      
            $hash = Yii::$app->getSecurity()->generatePasswordHash($model->password_hash);
            $model->password_hash = $hash;


            $model->updated_at = date("Y-m-d").' '.date("h:i:s a");
            $model->image = UploadedFile::getInstance($model, 'image');
            $model->image = file_get_contents($model->image->tempName);   
            
            $modelk->nim = $model->username;   

            if($model->save(false) && $modelk->save(false)){
                         
            }
            return $this->redirect(['updatemahasiswa', 'id' => $model->id]);
                                
        } else {
            return $this->render('updatemahasiswa', [
                'model' => $model,
                'modelk' => $modelk,
            ]);
        }
    }

    public function actionUpdateadmin($id)
    {
        
        $model = $this->findModel($id);        

        if ($model->load(Yii::$app->request->post()) && $modelk->load(Yii::$app->request->post())) {   
            $hash = Yii::$app->getSecurity()->generatePasswordHash($model->password_hash);
            $model->password_hash = $hash;                              
            $model->image = UploadedFile::getInstance($model, 'image');
            $model->image = file_get_contents($model->image->tempName);               

            $model->updated_at = date("Y-m-d").' '.date("h:i:s a");

            $model->save();

            return $this->redirect(['viewcek', 'id' => $model->id]);             
            }
             else {
            return $this->render('updateadmin', [
                'model' => $model,                
            ]);
        }
    }

        public function actionUpdatesatpam($id)
    {
        
        $model = $this->findModel($id);
        $modelk = Satpam::findOne($model->username);

        if ($model->load(Yii::$app->request->post()) && $modelk->load(Yii::$app->request->post())) {                      
            $hash = Yii::$app->getSecurity()->generatePasswordHash($model->password_hash);
            $model->password_hash = $hash;


            $model->updated_at = date("Y-m-d").' '.date("h:i:s a");
            $model->image = UploadedFile::getInstance($model, 'image');
            $model->image = file_get_contents($model->image->tempName);   
            
            $modelk->username = $model->username;   

            if($model->save(false) && $modelk->save(false)){
                         
            }
            return $this->redirect(['view', 'id' => $model->id]);
                                
        } else {
            return $this->render('updatesatpam', [
                'model' => $model,
                'modelk' => $modelk,
            ]);
        }
    }



     public function actionCreateasrama()
    {
        $model = new User();
        $modelk = new Keasramaan();

        if ($model->load(Yii::$app->request->post()) && $modelk->load(Yii::$app->request->post())) {
            $hash = Yii::$app->getSecurity()->generatePasswordHash($model->password_hash);
            $model->password_hash = $hash;
            $model->status="10";
            $model->generateAuthKey();
            $model->created_at = date("Y-m-d").' '.date("h:i:s a");
            $model->image = UploadedFile::getInstance($model, 'image');
            $model->image = file_get_contents($model->image->tempName);   

            $modelk->nid = $model->username;   
            $model->save(false);
            $modelk->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
                                
        } else {
            return $this->render('createasrama', [
                'model' => $model,
                'modelk' => $modelk,
            ]);
        }
    }

          public function actionCreatemahasiswa()
    {
        $model = new User();
        $modelk = new Mahasiswa();

        if ($model->load(Yii::$app->request->post()) && $modelk->load(Yii::$app->request->post())) {
            $hash = Yii::$app->getSecurity()->generatePasswordHash($model->password_hash);
            $model->password_hash = $hash;
            $model->status="10";
            $model->generateAuthKey();
            $model->created_at = date("Y-m-d").' '.date("h:i:s a");
            $model->image = UploadedFile::getInstance($model, 'image');
            $model->image = file_get_contents($model->image->tempName);   

            $modelk->nim = $model->username;   
            $model->save(false);
            $modelk->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
                                
        } else {
            return $this->render('createmahasiswa', [
                'model' => $model,
                'modelk' => $modelk,
            ]);
        }
    }


    public function actionCreatesatpam()
    {
        $model = new User();
        $modelk = new Satpam();

        if ($model->load(Yii::$app->request->post()) && $modelk->load(Yii::$app->request->post())) {
            $hash = Yii::$app->getSecurity()->generatePasswordHash($model->password_hash);
            $model->password_hash = $hash;
            $model->status="10";
            $model->generateAuthKey();
            $model->created_at = date("Y-m-d").' '.date("h:i:s a");
            $model->image = UploadedFile::getInstance($model, 'image');
            $model->image = file_get_contents($model->image->tempName);   

            $modelk->username = $model->username;   
            $model->save(false);
            $modelk->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
                                
        } else {
            return $this->render('createsatpam', [
                'model' => $model,
                'modelk' => $modelk,
            ]);
        }
    }


    //Bagian     di user, bukan di admin

    public function actionViewcek ($id){
        $user = User::findOne($id);

        if($user->role == 'Keasramaan'){
            $users = User::findOne($id);
            $ids =  $users->username;
            $Keasramaan = Keasramaan::findOne($ids);
           return $this->render('viewas', [
                'model' => $Keasramaan,
                'user' => $users,
             ]);
        }

        else if ($user->role == 'Admin') {
            return $this->render('viewadmin', [
                'model' => $this->findModel($id),
            ]);                
        }

        else if($user->role == 'Satpam'){
            $user = User::findOne($id);
            $ids =  $user->username;
            $Satpam = Satpam::findOne($ids);
           return $this->render('viewsat', [
                'model' => $Satpam,
                'user' => $user,
             ]);
        }

        else if ($user->role == 'Mahasiswa') {
            $user = User::findOne($id);
            $ids =  $user->username;
            $mahasiswa = Mahasiswa::findOne($ids);
           return $this->render('viewma', [
                'model' => $mahasiswa,
                'user' => $user,
             ]);
        }        
        else{
            $this->goHome();     
        }        
    }
    //KEASRASRAMA




    public function actionUpdateas($id)
    {
        
        $model = $this->findModel($id);
        $modelk = Keasramaan::findOne($model->username);

        if ($model->load(Yii::$app->request->post()) && $modelk->load(Yii::$app->request->post())) {                      
            $hash = Yii::$app->getSecurity()->generatePasswordHash($model->password_hash);
            $model->password_hash = $hash;
            $model->updated_at = date("Y-m-d").' '.date("h:i:s a");
            $model->image = UploadedFile::getInstance($model, 'image');
            $model->image = file_get_contents($model->image->tempName);       
            $model->username = $modelk->nid;             

            if($model->save(false) && $modelk->save(false)){
            return $this->redirect(['viewcek', 'id' => $model->id]);             
            }
            
                                
        } else {
            return $this->render('updateas', [
                'model' => $model,
                'modelk' => $modelk,
            ]);
        }
    }


    public function actionUpdatesat($id)
    {
        
        $model = $this->findModel($id);
        $modelk = Satpam::findOne($model->username);

        if ($model->load(Yii::$app->request->post()) && $modelk->load(Yii::$app->request->post())) {                                 
            $model->image = UploadedFile::getInstance($model, 'image');
            $model->image = file_get_contents($model->image->tempName);       
            $model->username = $modelk->username;             

            if($model->save(false) && $modelk->save(false)){
            return $this->redirect(['viewcek', 'id' => $model->id]);             
            }
            
                                
        } else {
            return $this->render('updatesat', [
                'model' => $model,
                'modelk' => $modelk,
            ]);
        }
    }


    public function actionUpdatema($id)
    {
        
        $model = $this->findModel($id);
        $modelk = Mahasiswa::findOne($model->username);

        if ($model->load(Yii::$app->request->post()) && $modelk->load(Yii::$app->request->post())) {                                 
            $model->image = UploadedFile::getInstance($model, 'image');
            $model->image = file_get_contents($model->image->tempName);               

            if($model->save(false) && $modelk->save(false)){
            return $this->redirect(['viewcek', 'id' => $model->id]);             
            }
                                            
        } else {
            return $this->render('updatema', [
                'model' => $model,
                'modelk' => $modelk,
            ]);
        }
    }


    

    //MAHASISWA


    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $user = $this->findModel($id); 
        
        if($user->role == 'Mahasiswa'){
        $Mahasiswa = Mahasiswa::findOne($user->username);   
        $Mahasiswa->delete();
        $user->delete();
        }
        elseif ($user->role == 'Keasramaan') {
            $keasramaan = Keasramaan::findOne($user->username);
            $keasramaan->delete();
            $user->delete();
        }
        elseif ($user->role == 'Satpam') {
            $satpam = Satpam::findOne($user->username);
            $satpam->delete();
            $user->delete();
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
