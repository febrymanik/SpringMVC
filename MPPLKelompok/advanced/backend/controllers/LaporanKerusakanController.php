<?php

namespace backend\controllers;

use Yii;
use backend\models\LaporanKerusakan;
use backend\models\Mahasiswa;
use backend\models\LaporanKerusakanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Filereportkerusakan;
use yii\web\UploadedFile;
use yii\bootstrap\Alert;
use yii\filters\AccessControl;

/**
 * LaporanKerusakanController implements the CRUD actions for LaporanKerusakan model.
 */
class LaporanKerusakanController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
         'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login','view', 'index', 'create', 'download', 'update', 'delete'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],            

            ], 
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LaporanKerusakan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LaporanKerusakanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LaporanKerusakan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LaporanKerusakan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LaporanKerusakan();    

        if ($model->load(Yii::$app->request->post())) {
        $mahasiswa = Mahasiswa::findOne(Yii::$app->user->identity->username);
        $model->namapelapor = $mahasiswa->nama;
        $model->nimpelapor = $mahasiswa->nim;
        $model->file= UploadedFile::getInstances($model, 'file');
            if($model->save()){
                    
                      if ($model->file != null) {
                        foreach ($model->file as $key => $files) {                            
                            $files->saveAs(Yii::getAlias('@filePath') . '/' . $files->name);
                            $file_report = new Filereportkerusakan();
                            $file_report->id_report = $model->nolaporan;
                            $file_report->namafile = $files->name;
                            $file_report->size = $files->size;
                            $file_report->save();
                        }
                    }
                
                }                             
            else {
                    Yii::$app->session->setFlash('error', 'Pengumuman Gagal Ditambahkan!');
            }

            return $this->redirect(['view', 'id' => $model->nolaporan]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    
    public function actionDownload($nama) {
        $path = Yii::getAlias('@filePath/') ;      
          $file = $path.  $nama;
        if (file_exists($file)) {
            Yii::$app->response->sendFile($file);
        } 
    }

    /**
     * Updates an existing LaporanKerusakan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->nolaporan]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing LaporanKerusakan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LaporanKerusakan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LaporanKerusakan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LaporanKerusakan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
