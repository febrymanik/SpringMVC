<?php

namespace backend\controllers;

use Yii;
use backend\models\Logkeluarmasuk;
use backend\models\LogKeluarMasukSearch;
use backend\models\Mahasiswa;
use backend\models\MahasiswaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\bootstrap\Alert;

/**
 * LogkeluarmasukController implements the CRUD actions for Logkeluarmasuk model.
 */
class LogkeluarmasukController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Logkeluarmasuk models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LogKeluarMasukSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Logkeluarmasuk model.
     * @param string $id
     * @return mixed
     */
    public function actionView($nim)
    {
        return $this->render('create', [
            'model' => $this->findModel($model->nim),
        ]);
    }

    /**
     * Creates a new Logkeluarmasuk model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
 public function actionCreate()
{
    $model = new Logkeluarmasuk();
    if($model->load(Yii::$app->request->post())){
        $mahasiswa = Mahasiswa::find()->where(['nim' => $model->nim])->one();
            if($mahasiswa != null){
                $cek = $this->findModel($model->nim);
                if($cek!==-1){
                    if($cek->status==='masuk'){                    
                        $cek->status ='keluar';
                        $cek->save();
                        return $this->render('create',[
                            'model'=>$cek,]);
                }
                else if($cek->status==='keluar')
                {
                    $cek->status='masuk';
                    $cek->save();
                            return $this->render('create',[
                    'model'=>$cek,]);//return $this->render('create', [   'model' => $model]);
                }
            }
            else{
                $model->status='keluar';
                $model->save();
                return $this->render('create',[
                    'model'=>$cek,]);        //return $this->render('create',[
                  //  'model'=>$model,]);
            }
        } else {
            echo "<script>alert('nim tidak terdaftar');</script>";
            return $this->render('create',['model'=>$model,$model->nim=null]);
        }
        }else{
                return $this->render('create',[
                    'model'=>$model,]);
            }
}



    /**
     * Finds the Logkeluarmasuk model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Logkeluarmasuk the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($nim)
    {
        $model = Logkeluarmasuk::find()->where(['nim' => $nim])->one();
        if ($model !== null) {
            return $model;
        } else {
            return -1;
        }
    }

     protected function findModelByNim($nim)
    {
        $model = Mahasiswa::find()->where(['nim' => $nim])->one();
        if ($model !== null) {
            return $model;
        } else {
            return -1;
        }
    }
}
