<?php
use yii\helpers\Html;
use backend\models\Mahasiswa;
use backend\models\Keasramaan;
?>
<header class="main-header">
        <!-- Logo -->
         <a href= "#" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>SIAS</b></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg">
                        <div class="sia-logo">DEL</div>
                        <div class="sia-text"> Sistem Informasi </br> Asrama</div>

                    </span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>

          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              
              <!-- Notifications: style can be found in dropdown.less -->
              
              <!-- Tasks: style can be found in dropdown.less -->
              
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
					           <?= Html::img('data:image/jpeg;base64,'.base64_encode(Yii::$app->user->identity->image), ['class' => 'user-image', 'alt'=>'User Image']) ?>
                  <span class="hidden-xs"><?=Yii::$app->user->identity->username?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                  <?= Html::img('data:image/jpeg;base64,'.base64_encode(Yii::$app->user->identity->image), ['class' => 'img-circle', 'alt'=>'User Image']) ?>
                    
                    <p>
                      <?=Yii::$app->user->identity->username?>
                      <small>
                      <?=Yii::$app->user->identity->role?>
                      </small>
                    </p>
                  </li>
                  <!-- Menu Body -->

                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">

                    <div class="pull-left">
                    <?= 
                    Html::a('Profile', 'index.php?r=user/viewcek&id='.Yii::$app->user->identity->id, ['class' => 'btn btn-default btn-flat']) ?>                      
                    </div>
                  
                    <div class="pull-right">                      
                       <?= 
                          
                            Html::a('Logout', ['site/logout'], ['class' => 'btn btn-default btn-flat', 'data-method'=>'post']) ;

                       ?>
                    </div>
                    
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>
            </ul>
          </div>
        </nav>
      </header>
