<?php

use adminlte\widgets\Menu;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
        </div>
       
        <?=
        Menu::widget(
               [
                    'options' => ['class' => 'sidebar-menu'],
                    'items' => [
                        ['label' => 'MENU UTAMA', 'options' => ['class' => 'header']],                        
                        [
                            'label' => 'Home',
                            'icon' => 'fa fa-dashboard',
                            'url' => ['site/index'], 
                            'active' => $this->context->route == 'site/index',                           
                        ],

                         [
                            'label' => 'User',
                            'icon' => 'fa fa-users',
                            'url' => '#',
                            'visible'=> Yii::$app->user->identity->role == "Admin",

                            'items' => [
                                [
                                    'label' => 'Tambah User',
                                    'icon' => 'fa fa-plus',
                                    'url' => ['/user/createasrama'],                                    
                                ],

                                [
                                    'label' => 'User',
                                    'icon' => 'fa fa-file-text',
                                    'url' => ['/user/index'],                                    
                                ],
                              
                            ]                        
                        ],
                        [
                            'label' => 'Mahasiswa',
                            'icon' => 'fa fa-users',
                            'url' => '#',
                            'visible'=> Yii::$app->user->identity->role == "Admin" || Yii::$app->user->identity->role == 'Keasramaan',
                            'items' => [                      
                                [
                                    'label' => 'Mahasiswa',
                                    'icon' => 'fa fa-file-text',
                                    'url' => ['mahasiswa/index'],                                    
                                ],
                              
                            ]                        
                        ],

                        [
                            'label' => 'Izin Bermalam',
                            'icon' => 'fa fa-file-text',
                            'url' => '#',
                            'visible' => Yii::$app->user->identity->role != "Satpam",
                            'items' => [

                                [   
                                    'label' => 'Request Izin Bermalam',
                                    'icon' => 'fa fa-plus',
                                    'url' => ['/izin-bermalam/createizinmahasiswa'],   
                                    'visible'=>  Yii::$app->user->identity->role == "Mahasiswa",                
                                ],

                                 [   
                                    'label' => 'Create Izin Bermalam',
                                    'icon' => 'fa fa-plus',
                                    'url' => ['/izin-bermalam/create'],   
                                    'visible'=> Yii::$app->user->identity->role == "Admin" || Yii::$app->user->identity->role == 'Keasramaan',                                 
                                ],

                                [   
                                    'label' => 'Log Izin Bermalam',
                                    'icon' => 'fa fa-file-text',
                                    'url' => ['/izin-bermalam/indexmahasiswa'],      
                                    'visible' => Yii::$app->user->identity->role == "Mahasiswa",        
                                ],

                                [
                                    'label' => 'Data Izin Bermalam ',
                                    'icon' => 'fa fa-file-text',
                                    'url' => ['izin-bermalam/index'],
                                    'visible'=> Yii::$app->user->identity->role == "Admin" || Yii::$app->user->identity->role == 'Keasramaan' || Yii::$app->user->identity->role == "Admin",                                    
                                ],                             
                            ]
                        ],


                   

                        [
                            'label' => 'Laporan Kerusakan',
                            'icon' => 'fa fa-tasks',
                            'url' => ['/'],             
                            'visible' => Yii::$app->user->identity->role != "Satpam",
                             'items' => [

                                [   
                                    'label' => 'Create Laporan',
                                    'icon' => 'fa fa-plus',
                                    'url' => ['/laporan-kerusakan/create'],                                    
                                    'visible' => Yii::$app->user->identity->role == "Mahasiswa", 
                                ],

                                [
                                    'label' => 'Data Laporan Kerusakan',
                                    'icon' => 'fa fa-file-text',
                                    'url' => ['laporan-kerusakan/index'],
                                    'visible' => Yii::$app->user->identity->role == "Keasramaan" || Yii::$app->user->identity->role == "Admin",                                    
                                ], 
                                                    
                            ]               
                        ],

                         [  
                            'label' => 'Pengumuman', 
                            'icon' => 'fa fa-bell', 
                            'url' => ['/'],       
                            'visible' => Yii::$app->user->identity->role != "Satpam",
                            'items' => [

                                [   
                                    'label' => 'Create Pengumuman',
                                    'icon' => 'fa fa-plus',
                                    'url' => ['/pengumuman/create'],     
                                    'visible' => Yii::$app->user->identity->role == "Keasramaan" || Yii::$app->user->identity->role == "Admin",                              
                                ],

                                [
                                    'label' => 'List Pengumuman',
                                    'icon' => 'fa fa-file-text',
                                    'url' => ['pengumuman/index'],
                                    'visible' => Yii::$app->user->identity->role == "Keasramaan" || Yii::$app->user->identity->role == "Admin",
                                    
                                ], 

                                [
                                    'label' => 'Pengumuman',
                                    'icon' => 'fa fa-file-text',
                                    'url' => ['pengumuman/index'],
                                    'visible' => Yii::$app->user->identity->role == "Mahasiswa",                         
                                ],                               
                            ]

                        ],
                        [
                            'label' => 'Log Izin Keluar',
                            'icon' => 'fa fa-plus',
                            'url' => ['/logkeluarmasuk/create'],
                            'visible' =>  Yii::$app->user->identity->role == "Satpam",
                            
                            
                        ],
                    ],
                ]
        )
        ?>
        
    </section>
    <!-- /.sidebar -->
</aside>
