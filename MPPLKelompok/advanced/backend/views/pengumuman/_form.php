<?php

use yii\helpers\Html;
use dosamigos\ckeditor\CKEditor;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\Pengumuman */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengumuman-form">


<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data','id'=>'tambah-pengumuman']]) ?>

    <?= $form->field($model, 'judul')->textInput(['maxlength' => true]) ?>


       <?=
    $form->field($model, 'isi_pengumuman')->widget(CKEditor::className(), [
        'options' => ['rows' => 5],
        'preset' => 'advanced'
    ])
    ?>

    
    <?= $form->field($model, 'ditujukan_untuk')->dropDownList([ 'Seluruh Mahasiswa' => 'Seluruh Mahaiswa', 'Asrama Pniel' => 'Asrama Pniel', 'Asrama Betfage' => 'Asrama Betfage', 'Asrama Kapernaum' => 'Asrama Kapernaum', 'Asrama Silo' => 'Asrama Silo', 'Asrama Antiokhia' => 'Asrama Antiokhia', 'Asrama Mahanaim' => 'Asrama Mahanaim', 'Asrama Mahanaim' => 'Asrama Mahanaim', 'Asrama Mamre' => 'Asrama Mamre', 'Asrama Nazareth' => 'Asrama Nazareth','Asrama Kana' => 'Asrama Kana',], ['prompt' => '']) ?>

     
    

     <?=
    $form->field($model, 'file[]')->widget(FileInput::classname(), [
        'options' => ['multiple' => 'true'],
        'pluginOptions' => [        
            'showUpload' => false,
            'browseLabel' => 'Browse File'
        ],
    ])
    ?>


    <div class="form-group">
       <center> <?= Html::submitButton($model->isNewRecord ? '<i class="glyphicon glyphicon-plus"></i> Tambah' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?></center>
    </div>

    <?php ActiveForm::end(); ?>

</div>
