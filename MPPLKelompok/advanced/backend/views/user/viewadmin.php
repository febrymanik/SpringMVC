<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Mahasiswa */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Mahasiswas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

echo ' <center><img  width="200" height="200" src="data:image/jpeg;base64,'.base64_encode($model->image).'"/></center>';
?>
<div class="mahasiswa-view">

<br>
<br>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [            
            'username',
            'email',
        ],
    ]) ?>

     <div class="form-group">        
       <?= Html::a('Update', ['updateadmin', 'id' => $model->id], ['class' => 'btn btn-primary']) ?> 
       <?= Html::a('Back', ['site/index'], ['class' => 'btn btn-success']) ?>
    </div>



</div>
