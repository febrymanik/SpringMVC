<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\User */

$this->title = 'Create Keasramaan';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">
   
    <?= $this->render('_form', [
        'model' => $model,
        'modelk' => $modelk,
    ]) ?>

</div>
