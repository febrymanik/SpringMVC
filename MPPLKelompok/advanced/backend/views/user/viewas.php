<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Keasramaan */

$this->title = $model->nid;
$this->params['breadcrumbs'][] = ['label' => 'Keasramaans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

echo ' <center><img  width="200" height="200" src="data:image/jpeg;base64,'.base64_encode($user->image).'"/></center>';
?>

<div class="keasramaan-view">

<br>
<br>
    <?= DetailView::widget([
        'model' => $model,
        
        'attributes' => [
            'nid',
            'nama',
            'no_telepon',
            [
                'label' => 'Email',
                'value' => $user->email,
            ],        
        ],
    ]) ?>

    <div class="form-group">        
        <?= Html::a('Update', ['updateas', 'id' => $user->id], ['class' => 'btn btn-primary']) ?>    
       <?= Html::a('Back', ['site/index'], ['class' => 'btn btn-success']) ?>
    </div>

</div>
