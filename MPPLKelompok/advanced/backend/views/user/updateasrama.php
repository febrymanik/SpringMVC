<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\User;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\models\User */

$this->title = 'Update Keasramaan: ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-update">
   
<br>
    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($modelk, 'nama')->textInput(['maxlength' => true]) ?>   

    <?= $form->field($modelk, 'no_telepon')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password_hash')->textInput(['maxlength' => true])?>    

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image')->fileInput()?>  
    

    <?= $form->field($model,'role')->dropDownList(['Keasramaan'=>'Keasramaan', 'Mahasiswa'=> 'Mahasiswa', 'Satpam' => 'Satpam'],['prompt' => '--Select Role--'])?>

    <div class="form-group">
        <?= Html::submitButton( 'Update' , ['class' => 'btn btn-primary' ]) ?>
       <?= Html::a('Back', ['view', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>



</div>
