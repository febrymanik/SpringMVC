<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model backend\models\LogKeluarAsrama */

$this->title = 'Create Log Keluar Masuk';
$this->params['breadcrumbs'][] = ['label' => 'Log Keluar Asramas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-keluar-asrama-create">
    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>

<div class="log-keluar-asrama-view">
    
     <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nim',
            //'nama',
            'status',
        ],
    ]) ?>

</div>

