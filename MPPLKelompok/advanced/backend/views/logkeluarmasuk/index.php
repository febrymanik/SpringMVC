<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\LogKeluarMasukSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Logkeluarmasuks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="logkeluarmasuk-index">

    
<?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nim')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::a('Create Logkeluarmasuk', ['create', 'id' => $model->nim, 'class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>


   
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nim',
            'nama',
            'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
