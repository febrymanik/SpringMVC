<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MahasiswaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mahasiswa';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="mahasiswa-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'nim',
            'nama',
            'notlpon',
            'alamat',
            'idnilai',
            // 'lantai',
            // 'kamar',
            // 'notlponortu',
            // 'jenisKelamin',

            [        
              'class' => 'yii\grid\ActionColumn',
              'header' => 'Actions',
              'headerOptions' => ['style' => 'color:#337ab7'],
              'template' => '{view} {setnilai}',
              'buttons' => [
                'view' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => Yii::t('app', 'view'),
                    ]);
                },

                 'setnilai' => function ($url, $model) {

                return Yii::$app->user->identity->username == 'Keasramaan'?
                Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => Yii::t('app', 'setnilai'),
                ]):'';
            },
              ],

              'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'view') {
                    $url ='index.php?r=mahasiswa/view&id='.$model->nim;
                    return $url;
                }
                 if ($action === 'setnilai') {
                    $url = ['updatenilai', 'id' => $model->nim];
                return $url;
            }

            
              }
          ],
        ],
    ]); ?>
</div>
