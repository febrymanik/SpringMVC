<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Mahasiswa */
/* @var $form yii\widgets\ActiveForm */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Mahasiswa', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="mahasiswa-form">

      <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nim',
            'nama',
            'notlpon',
            'alamat',            
            'lantai',
            'kamar',
            'notlponortu',
            'jenisKelamin',
        ],
    ]) ?>


    <?php $form = ActiveForm::begin(); ?>


    
      <?= $form->field($model,'idnilai')->dropDownList(['A'=>'A', 'B'=> 'B', 'B' => 'B', 'C' => 'C', 'D' => 'D', 'E' => 'E'])?>


    <div class="form-group">
        <?= Html::submitButton('Submit',['class' => 'btn btn-success' ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
