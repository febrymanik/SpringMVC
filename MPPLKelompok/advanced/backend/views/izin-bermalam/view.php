<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\IzinBermalam */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Izin Bermalam', 'url' => ['indexmahasiswa']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="izin-bermalam-view">
    

    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'no',
            'nama',
            'nim',            
            'no_telepon_orangtua',
            'tujuan_ib',
            'tanggal_keberangkatan',
            'tanggal_kembali',
            'ib_membawa_laptop',
            'keperluan_ib',
            'nid_keasramaan',
            'status',
        ],
    ]) ?>

</div>
