<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\IzinBermalamSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="izin-bermalam-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'no') ?>

    <?= $form->field($model, 'nama') ?>

    <?= $form->field($model, 'nim') ?>

    <?= $form->field($model, 'kelas') ?>

    <?= $form->field($model, 'no_telepon_orangtua') ?>

    <?php // echo $form->field($model, 'tujuan_ib') ?>

    <?php // echo $form->field($model, 'tanggal_keberangkatan') ?>

    <?php // echo $form->field($model, 'tanggal_kembali') ?>

    <?php // echo $form->field($model, 'ib_membawa_laptop') ?>

    <?php // echo $form->field($model, 'keperluan_ib') ?>

    <?php // echo $form->field($model, 'nid_keasramaan') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
