<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SatpamSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Satpams';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="satpam-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Satpam', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'username',
            'nama',
            'notelepon',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
