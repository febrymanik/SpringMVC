<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\LaporanKerusakan */

$this->title = 'Create Laporan Kerusakan';
$this->params['breadcrumbs'][] = ['label' => 'Laporan Kerusakans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="laporan-kerusakan-create">
   

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
