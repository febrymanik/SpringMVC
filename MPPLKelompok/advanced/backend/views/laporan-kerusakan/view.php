<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\Filereportkerusakan;
/* @var $this yii\web\View */
/* @var $model backend\models\Laporankerusakan */

$this->title = 'Laporan Kerusakan Asrama';
$this->params['breadcrumbs'][] = ['label' => 'Laporankerusakans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

 <div class="col-md-14">
            <div class="box box-solid" >

                <div class="box-body">
                

                                        
                    <?php
                        
                            echo "<p>Oleh  :".$model->namapelapor."</p>";                        
                        
                    ?>
                   
                    <?= $model->keterangan ?> 
                    
                                    
                    <?php 
                    $filess = Filereportkerusakan::find()->where(['id_report' => $model->nolaporan])->all();                    
                    if($filess != null){
                        echo " <table class='table'>
                        <thead>
                        <th>Nama File:</th>
                        <th>Size:</th>
                        </thead>
                        <tbody>";

                        echo     "<tr>";
                        foreach ($filess as $key => $filee){ 
                        echo     "<td>" . Html::a(Html::encode($filee->namafile), ['download', 'nama' => $filee->namafile])."</td>";
                        echo    "<td>".number_format($filee->size / 1024, 2) . ' KB'."</td>";                           
                        
                        echo       "</tr>";
                        
                        }
                        echo "</tbody>";                            
                        echo "</table>";                    
                    
                }
                    ?>
                    


                </div><!-- /.box-body -->
            </div><!-- /. box -->
        </div><!-- /.col -->    