<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Laporankerusakan */

$this->title = 'Update Laporankerusakan: ' . $model->nolaporan;
$this->params['breadcrumbs'][] = ['label' => 'Laporankerusakans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nolaporan, 'url' => ['view', 'id' => $model->nolaporan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="laporankerusakan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
