<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\LaporanKerusakanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Laporankerusakan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="laporankerusakan-index">
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'nolaporan',
            'namapelapor',
            'nimpelapor',            
            [        
              'class' => 'yii\grid\ActionColumn',
              'header' => 'Actions',
              'headerOptions' => ['style' => 'color:#337ab7'],
              'template' => '{view} ',
              'buttons' => [
                'view' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => Yii::t('app', 'view'),
                    ]);
                },                                
        
            ],

              'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'view') {
                    $url = ['view', 'id'=>$model->nolaporan];
                    return $url;
                } 
                
                    
            }
          ],
        ],
    ]); ?>
</div>
