<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "satpam".
 *
 * @property string $username
 * @property string $nama
 * @property string $notelepon
 */
class Satpam extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'satpam';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'nama', 'notelepon'], 'required'],
            [['username', 'nama', 'notelepon'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => 'Username',
            'nama' => 'Nama',
            'notelepon' => 'Notelepon',
        ];
    }
}
