<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "keasramaan".
 *
 * @property string $nid
 * @property string $nama
 * @property string $no_telepon
 *
 * @property IzinBermalam[] $izinBermalams
 * @property Pengumuman[] $pengumumen
 */
class Keasramaan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'keasramaan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nid'], 'required'],
            [['nid'], 'string', 'max' => 10],
            [['nama', 'no_telepon'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nid' => 'Nid',
            'nama' => 'Nama',
            'no_telepon' => 'No Telepon',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIzinBermalams()
    {
        return $this->hasMany(IzinBermalam::className(), ['nid_keasramaan' => 'nid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPengumumen()
    {
        return $this->hasMany(Pengumuman::className(), ['nid_keasramaan' => 'nid']);
    }
}
