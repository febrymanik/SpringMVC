<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "laporankerusakan".
 *
 * @property integer $nolaporan
 * @property string $namapelapor
 * @property string $nimpelapor
 * @property string $keterangan
 *
 * @property Mahasiswa $nimpelapor0
 */
class Laporankerusakan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file;
    public static function tableName()
    {
        return 'laporankerusakan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['namapelapor'], 'string', 'max' => 30],
            [['nimpelapor'], 'string', 'max' => 10],
            [['keterangan'], 'string', 'max' => 9999],
            [['nimpelapor'], 'exist', 'skipOnError' => true, 'targetClass' => Mahasiswa::className(), 'targetAttribute' => ['nimpelapor' => 'nim']],
            [['file'], 'file','maxFiles' => 5],
        ];

    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nolaporan' => 'Nolaporan',
            'namapelapor' => 'Namapelapor',
            'nimpelapor' => 'Nimpelapor',
            'keterangan' => 'Keterangan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNimpelapor0()
    {
        return $this->hasOne(Mahasiswa::className(), ['nim' => 'nimpelapor']);
    }
}
