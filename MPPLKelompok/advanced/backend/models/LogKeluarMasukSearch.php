<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "logkeluarmasuk".
 *
 * @property integer $no
 * @property string $nama
 * @property string $nim
 * @property string $waktu_keluar
 * @property string $waktu_kembali
 * @property string $status
 *
 * @property Mahasiswa $nim0
 */
class LogKeluarMasukSearch extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logkeluarmasuk';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no', 'nim'], 'required'],
            [['no'], 'integer'],
            [['waktu_keluar', 'waktu_kembali'], 'safe'],
            [['nama'], 'string', 'max' => 30],
            [['nim', 'status'], 'string', 'max' => 10],
            [['nim'], 'exist', 'skipOnError' => true, 'targetClass' => Mahasiswa::className(), 'targetAttribute' => ['nim' => 'nim']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'no' => 'No',
            'nama' => 'Nama',
            'nim' => 'Nim',
            'waktu_keluar' => 'Waktu Keluar',
            'waktu_kembali' => 'Waktu Kembali',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNim0()
    {
        return $this->hasOne(Mahasiswa::className(), ['nim' => 'nim']);
    }
}
