<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\IzinBermalam;

/**
 * IzinBermalamSearch represents the model behind the search form about `backend\models\IzinBermalam`.
 */
class IzinBermalamSearch extends IzinBermalam
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no'], 'integer'],
            [['nama', 'nim', 'no_telepon_orangtua', 'tujuan_ib', 'tanggal_keberangkatan', 'tanggal_kembali', 'ib_membawa_laptop', 'keperluan_ib', 'nid_keasramaan', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = IzinBermalam::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'no' => $this->no,
            'tanggal_keberangkatan' => $this->tanggal_keberangkatan,
            'tanggal_kembali' => $this->tanggal_kembali,
        ]);

        $query->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'nim', $this->nim])            
            ->andFilterWhere(['like', 'no_telepon_orangtua', $this->no_telepon_orangtua])
            ->andFilterWhere(['like', 'tujuan_ib', $this->tujuan_ib])
            ->andFilterWhere(['like', 'ib_membawa_laptop', $this->ib_membawa_laptop])
            ->andFilterWhere(['like', 'keperluan_ib', $this->keperluan_ib])
            ->andFilterWhere(['like', 'nid_keasramaan', $this->nid_keasramaan])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }

    public function searchlogin($params)
    {
        $query = IzinBermalam::find()->where(['nim' =>Yii::$app->user->identity->username]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'no' => $this->no,
            'tanggal_keberangkatan' => $this->tanggal_keberangkatan,
            'tanggal_kembali' => $this->tanggal_kembali,
        ]);

        $query->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'nim', $this->nim])        
            ->andFilterWhere(['like', 'no_telepon_orangtua', $this->no_telepon_orangtua])
            ->andFilterWhere(['like', 'tujuan_ib', $this->tujuan_ib])
            ->andFilterWhere(['like', 'ib_membawa_laptop', $this->ib_membawa_laptop])
            ->andFilterWhere(['like', 'keperluan_ib', $this->keperluan_ib])
            ->andFilterWhere(['like', 'nid_keasramaan', $this->nid_keasramaan])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
