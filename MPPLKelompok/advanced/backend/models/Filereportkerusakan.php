<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "filereportkerusakan".
 *
 * @property integer $id
 * @property integer $id_report
 * @property string $namafile
 * @property integer $size
 */
class Filereportkerusakan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'filereportkerusakan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_report', 'namafile', 'size'], 'required'],
            [['id_report', 'size'], 'integer'],
            [['namafile'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_report' => 'Id Report',
            'namafile' => 'Namafile',
            'size' => 'Size',
        ];
    }
}
